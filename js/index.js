$(function () {
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
      interval: 2000
    });

    $('#contacto').on('show.bs.modal', function (event) {
      console.log('el modal contacto se está mostrando');

      $('#contactoBtn1').removeClass('btn-outline-success');
      $('#contactoBtn1').addClass('btn-secondary');
      $('#contactoBtn1').prop('disabled', true);

      $('#contactoBtn2').removeClass('btn-outline-success');
      $('#contactoBtn2').addClass('btn-secondary');
      $('#contactoBtn2').prop('disabled', true);

      $('#contactoBtn3').removeClass('btn-outline-success');
      $('#contactoBtn3').addClass('btn-secondary');
      $('#contactoBtn3').prop('disabled', true);

      $('#contactoBtn4').removeClass('btn-outline-success');
      $('#contactoBtn4').addClass('btn-secondary');
      $('#contactoBtn4').prop('disabled', true);

      $('#contactoBtn5').removeClass('btn-outline-success');
      $('#contactoBtn5').addClass('btn-secondary');
      $('#contactoBtn5').prop('disabled', true);

      $('#contactoBtn6').removeClass('btn-outline-success');
      $('#contactoBtn6').addClass('btn-secondary');
      $('#contactoBtn6').prop('disabled', true);

      $('#contactoBtn7').removeClass('btn-outline-success');
      $('#contactoBtn7').addClass('btn-secondary');
      $('#contactoBtn7').prop('disabled', true);

      $('#contactoBtn8').removeClass('btn-outline-success');
      $('#contactoBtn8').addClass('btn-secondary');
      $('#contactoBtn8').prop('disabled', true);

    });

    $('#contacto').on('shown.bs.modal', function (event) {
      console.log('el modal contacto se mostró');
    });

    $('#contacto').on('hide.bs.modal', function (event) {
      console.log('el modal contacto se está ocultando');
    });

    $('#contacto').on('hidden.bs.modal', function (event) {
      console.log('el modal contacto se ocultó');

      $('#contactoBtn1').removeClass('btn-secondary');
      $('#contactoBtn1').addClass('btn-outline-success');
      $('#contactoBtn1').prop('disabled', false);

      $('#contactoBtn2').removeClass('btn-secondary');
      $('#contactoBtn2').addClass('btn-outline-success');
      $('#contactoBtn2').prop('disabled', false);

      $('#contactoBtn3').removeClass('btn-secondary');
      $('#contactoBtn3').addClass('btn-outline-success');
      $('#contactoBtn3').prop('disabled', false);

      $('#contactoBtn4').removeClass('btn-secondary');
      $('#contactoBtn4').addClass('btn-outline-success');
      $('#contactoBtn4').prop('disabled', false);

      $('#contactoBtn5').removeClass('btn-secondary');
      $('#contactoBtn5').addClass('btn-outline-success');
      $('#contactoBtn5').prop('disabled', false);

      $('#contactoBtn6').removeClass('btn-secondary');
      $('#contactoBtn6').addClass('btn-outline-success');
      $('#contactoBtn6').prop('disabled', false);

      $('#contactoBtn7').removeClass('btn-secondary');
      $('#contactoBtn7').addClass('btn-outline-success');
      $('#contactoBtn7').prop('disabled', false);

      $('#contactoBtn8').removeClass('btn-secondary');
      $('#contactoBtn8').addClass('btn-outline-success');
      $('#contactoBtn8').prop('disabled', false);

    });

  });